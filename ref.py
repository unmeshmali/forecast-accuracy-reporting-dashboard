#!/usr/bin/env python
# coding: utf-8

# In[2]:


import csv
import gzip
from io import BytesIO
from io import StringIO
import pandas as pd
import datetime
import boto3
from gzip import GzipFile
from getpass import getpass
from datetime import datetime as dt
import psycopg2
import calendar

import plotly.graph_objs as go
import plotly.offline as pyo

import datetime as dt
import sissim2
import pytz


# In[3]:



tz = 'Asia/Tokyo'


client = boto3.client('s3')
resource = boto3.resource('s3')


# In[4]:
Date = pd.to_datetime(2019,11,13)
Time = 12

my_bucket = resource.Bucket('demo-sunverge-forecast-power-energy')
#Key_Date = input("Please enter the date and time of forecast (e.g.2019-11-24T12:00:00 ) : ")

Key_Date = str(Date) + 'T' + str(Time) + ':00:00'

Key = ('clientid="5d995db9-8761-4379-ba0f-a8373184a1db"/createdtime=' + Key_Date + '/power-energy-forecast-' + Key_Date + 'Z.csv.gz')

# In[5]:


#Key = 'clientid="24d1bcb3-8857-4341-beac-04aa97941787"/createdtime=2019-11-24T12:00:00/power-energy-forecast-2019-11-24T12:00:00Z.csv.gz'
client = boto3.client('s3')
resource = boto3.resource('s3')
my_bucket = resource.Bucket('demo-sunverge-forecast-power-energy')
obj = client.get_object(Bucket = 'demo-sunverge-forecast-power-energy', Key = Key)


# In[6]:


with gzip.open(obj['Body'], mode = 'rt') as f:
    forecast_df = pd.read_csv(f)

forecast_df['time'] = pd.to_datetime(forecast_df['time'])

forecast_df['time'] = forecast_df['time'].dt.tz_convert(tz)

#forecast_df['time'] = forecast_df['time'] + dt.timedelta(hours = 9)
print(forecast_df.dtypes)
            # The purpose of the lines below was to check the plot of the above data frame. It was beautiful. 
            # print(forecast_df.head())
            # traces  = go.Scatter(x = forecast_df['time'],
            #            y = forecast_df['load']
            #     )
            # data = [traces]
            # layout = go.Layout(title = 'PV Graph test')
            # fig = go.Figure(data = data, 
            #                 layout = layout)
            
            # pyo.plot(fig)


import time


# In[7]:
"""

forecast_df.head()
forecast_df['load'] = forecast_df['load']*1000
forecast_df['pv'] = forecast_df['pv']*1000
forecast_df.set_index('time', inplace = True)
forecast_df.index = pd.to_datetime(forecast_df.index)
forecast_df = forecast_df.tz_localize(None)


forecast_df = forecast_df.resample('1h').mean()
print(forecast_df.head())

"""
# In[ ]:
 
Date = datetime.date.today()

Time = 12
        # Function to get the actual data from the database. Making sure that it is 15min averaged. 
host = 'prod-sunverge-redshift.cvfma8pk1vjx.us-east-1.redshift.amazonaws.com'
port = 5439
dbname = 'datamart'
username = 'jwang8'
password = 'cyWang880'

conn = sissim2.redshift_connect(host, port, dbname, username, password)

start_date = str(Date) + " " + str(Time) + ":00:00"
print(Date)
print(start_date)
end_date = str(Date + datetime.timedelta(days = 1)) + " " + str(Time) + ":00:00"
print(end_date)
tenantname = 'consumersenergy'
displayname = 'CE_ES001_S.Devries'

results_df = []
def get_actual_data(conn, start_date, tenantname, end_date, displayname):
    # Start the connection and give it a name
    low_res_cursor = conn.cursor()
    
    low_res_cursor.execute("""select date_trunc('hour', polldate) + date_part('minute', polldate)::int / 15 * interval '15 min' as utc_timestamp,
    trunc(polldate) as utc_date,
    date_part('hour', polldate) as utc_hour,
    15 * (date_part('minute', polldate)::int/15) as utc_15min,
    avg(load_power) as load_power,
    avg(pv_power) as pv_power
    from dessdevicedata ddd
    join dmc_devices d on ddd.clientid = d.clientid
    join dmc_units u on d.cabinetserial = u.cabinetserial
    where displayname = %s
    and polldate >= %s
    and polldate < %s
    group by utc_timestamp, utc_date, utc_hour, utc_15min
    order by utc_timestamp, utc_date, utc_hour, utc_15min;""",[displayname ,
                                                             start_date,
                                                            end_date,])
    results = low_res_cursor.fetchall()
    results_df = pd.DataFrame(list(results), columns = ['utc_timestamp', 'utc_date', 'utc_hour', 'utc_15min', 'actual_load_power', 'actual_pv_power']) 

    return results_df

results_df = get_actual_data(conn, start_date, tenantname, end_date, displayname)
results_df['local_time'] = results_df['utc_timestamp'].dt.tz_localize(tz)

print(results_df.dtypes)


final_joined_df = pd.concat([forecast_df, results_df.rename(columns = {'local_time' : 'time'})])
print(final_joined_df.dtypes)

# In[ ]:





# In[ ]:


# conn = psycopg2.connect(host="prod-sunverge-redshift.cvfma8pk1vjx.us-east-1.redshift.amazonaws.com", 
#                         port=5439, dbname="datamart" ,user = 'jwang8' , password = 'cyWang880')
# print("connected!")
# cur = conn.cursor()
# client = input("Enter the name of client (Mr Ishibashi or Intelligent House : ")

# if client == 'Mr Inshibashi':
#     clientid = '24d1bcb3-8857-4341-beac-04aa97941787'
# elif client == 'Intelligent House':
#     clientid = '5d995db9-8761-4379-ba0f-a8373184a1db'

# cur.execute("""select polldate,
#                       load_power,
#                       pv_power,
#                       site_demand_power
#                 from dessdevicedata
#                 where clientid = %s
#                 and polldate >= '2019-11-12'
#                 """, (client,))
# results = cur.fetchall()


# In[ ]:


# name4 = input("Please enter a name ")
# name4age = int(input("Please enter {}'s age: ".format(name4)))


# In[ ]:





# In[ ]:





# In[ ]:





