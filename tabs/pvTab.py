import dash
import plotly
import dash_core_components as dcc
import dash_html_components as html 
import dash_bootstrap_components as dbc 
import dash_table
import pandas as pd
from dash.dependencies import Input, Output
import numpy as np



from app import app 
from database import getData

columns = ['time', 'load', 'pv', 'dischargeCapacity', 'chargeCapacity',
       'maxDischargeRate', 'maxChargeRate']

Data = np.array([np.arange(192)]*7).T
forecast_df = pd.DataFrame(Data, columns = columns)

layout = html.Div([
        html.Br(),
        html.Br(),
        dash_table.DataTable(id = 'pv-data',
                 columns = [{'name' : i, 'id' : i} for i in forecast_df.columns],
                 data = forecast_df.to_dict('records'))
    ])