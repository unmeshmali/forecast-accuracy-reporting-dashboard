import dash
import plotly
import dash_core_components as dcc
import dash_html_components as html 
import dash_bootstrap_components as dbc 
import dash_table
import pandas
from dash.dependencies import Input, Output, State
import boto3
import gzip
from gzip import GzipFile

from app import app
import pandas as pd
from tabs import loadTab, pvTab
from database import getData
import numpy as np
import psycopg2
from datetime import datetime as dt


df = pd.read_csv('/Users/unmeshmali/Desktop/Unmesh/Dash/forecastDash/database/names.csv')


# theme =  {
#     'dark': True,
#     'detail': '#007439',
#     'primary': '#00EA64',
#     'secondary': '#6E6E6E', 
# }

# columns = ['time', 'load', 'pv', 'dischargeCapacity', 'chargeCapacity',
#        'maxDischargeRate', 'maxChargeRate']

# Data = np.array([np.arange(192)]*7).T
# forecast_df = pd.DataFrame(Data, columns = columns) 

# layout = html.Div([

#     html.H1("Wax On Wax Off"), 
#     dbc.Row([
#         dbc.Col(html.Div(["Column_1"])),
#         dbc.Col(html.Div(["Column_2"])),
#         dbc.Col(html.Div(["Column_3"]))
#         ])

#     ])

layout = html.Div(
    [
        html.H1(["Forecast Accuracy Check Dashboard"],
                    style = {'backgroundColor': 'black',
                            'textAlign': 'center',
                            'color': 'white',
                            "height" : '6vh',
                            "marginTop" : '1vh',
                            "marginBottom" : '3vh',

                    }),
        html.Div([
                         ]),

        dbc.Row(
        [
            dbc.Col(    
                [html.Div(className="div-for-dropdown",
                    children = [
                        html.P("Tenant", className= "dark-theme-control"),
                            
                            dcc.Dropdown(
                                id = 'my-tenant',
                                options = getData.make_dropdown_list(df),
                                placeholder = 'select tenant',
                                className = "dark-theme-control"
                                        )
                        ]#,
                        # style = {'width': '80%',
                        #         'display': 'inline-block',
                        #         'verticalAlign': 'middle'
                        #         },
                        ),
                html.Br(),
                # html.Div([
                #          ]),

                html.Div([
                        html.P("Client", className= "dark-theme-control"),
                        dcc.Dropdown(
                            id = 'my-client',
                            placeholder = 'select Client',
                            className = "dark-theme-control"
                        )],
                        style = {'width': '80%',
                                'display': 'inline-block',
                                'verticalAlign': 'middle'
                        },
                        ),
                html.Br(),
                html.Br(),

                html.Div([
                        html.P("Date", className = "dark-theme-control"),
                        dcc.DatePickerSingle(
                            id = 'my-date',
                            placeholder = 'Select Date',
                            min_date_allowed = dt(2019,1,1),
                            max_date_allowed = dt(2020,3,1),
                            initial_visible_month = dt(2019,9,25),
                            className = "dark-theme-control"
                        )],
                        style = {'right': '0px',
                                'width': '30%',
                                'display': 'inline-block'
                        },
                        ),
                html.Br(),
                html.Br(),

                html.Div([
                          html.P("Time", className = "dark-theme-control"),
                          dcc.Input(
                            id = 'my-time',
                            placeholder = 'Enter hour of day', 
                            min = 0,
                            max = 23,
                            step = 1,
                            type = 'number',
                            className = "dark-theme-control"
                            )
                        ],
                        style = {'right': '0px',
                                'width': '100%',
                                'display': 'inline-block'
                        },


                        ),
                 html.Br(),
                html.Br(),
                html.Div([
                    html.P("Select Hours Ahead", className = "dark-theme-control"),
                    dcc.Input(
                        id = 'hours-ahead',
                        type = 'number',
                        min = 0,
                        max = 48, 
                        step = 0.25)
                    ],
                    style = {'right': '0px',
                                'width': '100%',
                                'display': 'inline-block'},
                    ),
                html.Br(),
                html.Br(),
                html.Div([
                    html.Button('Update', id = 'update-inputs', n_clicks = 0)
                    ]),
                
                ], width = {"size" : 2, "offset" : 1, 'color': 'yellow'},

                ),

            
            dbc.Col([html.Div([
                        dcc.Tabs(id="tabs", 
                            children = [
                                dcc.Tab(label='Load Check', value='loadTab'),
                                dcc.Tab(label='PV Check', value='pvTab'),
                                    ]
                                ),
                        html.Br(),
                        html.Br(),
                                 html.Div([
                                     html.Div([html.P(id = 'rmse',
                                            style = {'fontSize' : 25})]),
                                     dcc.Graph(id = 'tabs-content')
                                     ])
                                 
            ])
        ])   
    ]
)
        ])


# @app.callback(Output('my-client', 'options'),
#               [Input('my-tenant', 'value')])    

# def make_displayname_drop_list(tenantName):
#     displayChoice = []
#     df_sub = df[df.tenantname == tenantName]
#     for j in df_sub.displayname:
#         dp = {'label': j, 'value' : j}
#         displayChoice.append(dp)
#     return displayChoice




# @app.callback(Output('tabs-content', 'children'),
#                 [Input('update-inputs', 'n_clicks')],
#                 state = [State('my-client', 'children'),
#                 State('my-date', 'date'),
#                 State('my-time', 'value'),
#                 State('hours-ahead', 'value'),
#                 State('tabs', 'value')],
#                 )

# def make_load_chart(n_clicks, Client, Date, Time, HoursAhead, tab):
#     if n_clicks:        
#         client = boto3.client('s3')
#         resource = boto3.resource('s3')
#         my_bucket = resource.Bucket('demo-sunverge-forecast-power-energy')
#         Clientid = []
#         dff = forecast_df
#         if Client == 'Tokyo Gas 1':
#             Clientid.append("24d1bcb3-8857-4341-beac-04aa97941787") # Mr. Ishibashi house
#         else : 
#             Clientid.append("5d995db9-8761-4379-ba0f-a8373184a1db") # Intelligent House   
        
#         #date = Date.strftime('%Y-%m-%d')
        
#         Key_Date = str(Date) + 'T' + str(Time) + ':00:00'
#         #Key_Date = input("Please enter the date and time of forecast (e.g.2019-11-24T12:00:00 ) : ")
        
#         Key = ("clientid=" + '"' + str(Clientid[0]) + '"' + "/createdtime=" + str(Key_Date) + "/power-energy-forecast-" + str(Key_Date) + 'Z.csv.gz')
#         obj = client.get_object(Bucket = 'demo-sunverge-forecast-power-energy', Key = Key)
       
#         with gzip.open(obj['Body'], mode = 'rt') as f:
#             dff = pd.read_csv(f)
        
        
#         if tab == 'loadTab': 
#             return dash_table.DataTable(id = 'table', 
#                                          columns = [{'name' : j, 'id' : j} for j in forecast_df.columns],
#                                          data = dff.to_dict('records'),)
#         elif tab == 'pvTab':
#             return 0



