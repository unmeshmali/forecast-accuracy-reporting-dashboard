import psycopg2
import datetime
import csv
import boto3
import gzip
import pandas as pd
import numpy as np
import pytz
import getpass


#Define efficiency functions
def pv_to_batt_eff(pv_power):
    if pv_power < 250:
        eff = 0.7 + (pv_power - 0) * (0.9 - 0.7) / float(250 - 0)
    elif pv_power < 750:
        eff = 0.9 + (pv_power - 250) * (0.945 - 0.9) / float(750 - 250)
    elif pv_power < 2000:
        eff = 0.945
    else:
        eff = 0.945 + (pv_power - 2000) * (0.9 - 0.945) / float(5000 - 2000)
    return eff

def grid_to_load_eff(critical_load_power):
    if critical_load_power < 200:
        eff = 0.5 + (critical_load_power - 0) * (0.91 - 0.5) / float(200 - 0)
    elif critical_load_power < 800:
        eff = 0.91 + (critical_load_power - 200) * (0.988 - 0.91) / float(800 - 200)
    elif critical_load_power < 6500:
        eff = 0.988 + (critical_load_power - 800) * (0.998 - 0.988) / float(6500 - 2000)
    else:
        eff = 0.998
    return eff

def grid_to_batt_eff(grid_in_power):
    if grid_in_power < 500:
        eff = 0.6 + (grid_in_power - 0) * (0.9 - 0.6) / float(500 - 0)
    else:
        eff = 0.9
    return eff

def pv_to_grid_eff(pv_power):
    if pv_power < 500:
        eff = 0.2 + (pv_power - 0.2) * (0.84 - 0.2) / float(500 - 0)
    elif pv_power < 1000:
        eff = 0.84 + (pv_power - 500) * (0.88 - 0.84) / float(1000 - 500)
    elif pv_power < 1750:
        eff = 0.88 + (pv_power - 1000) * (0.93 - 0.88) / float(1750 - 1000)
    else:
        eff = 0.93 + (pv_power - 1750) * (0.85 - 0.93) / float(5000 - 1750)
    return eff

def batt_to_grid_eff(grid_out_power):
    if grid_out_power < 500:
        eff = 0.2 + (grid_out_power - 0.2) * (0.84 - 0.2) / float(500 - 0)
    elif grid_out_power < 1000:
        eff = 0.84 + (grid_out_power - 500) * (0.88 - 0.84) / float(1000 - 500)
    elif grid_out_power < 1750:
        eff = 0.88 + (grid_out_power - 1000) * (0.95 - 0.88) / float(1750 - 1000)
    else:
        eff = 0.95
    return eff

#Return peak period type for a given local timestamp
def peakflag(local_datetime, peakperiodlist):
    peakflag = None
    for peakperiod in peakperiodlist:
        if local_datetime.hour >= peakperiod[0] and local_datetime.hour <= peakperiod[1]:
            peakflag = peakperiod[2]
    if peakflag is None:
        print("Error: Peak period type undefined for local timestamp " + str(local_datetime))
        exit()
    else:
        return peakflag 
    
#Return charge period type for a given local timestamp
def chargecheck(local_datetime, chargeperiodlist):
    for chargeperiod in chargeperiodlist:
        if local_datetime.hour >= chargeperiod[0] and local_datetime.hour < chargeperiod[1]:
            chargeresult = chargeperiod[2]
    if chargeresult is None:
        print("Error: Charge period type undefined for local timestamp " + str(local_datetime))
        exit()
    else:
        return chargeresult

def redshift_connect(host, port, dbname, username, password):
    #Check we have all the variable information, and request input from user if necessary
    if host is None:
        host = raw_input("Enter Redshift host: ")
    
    if port is None:
        port = raw_input("Enter Redshift port: ")
    
    if dbname is None:
        dbname = raw_input("Enter Redshift database name: ")
        
    if username is None:
        username = raw_input("Enter Redshift username: ")
    
    if password is None:
        password = getpass.getpass("Enter Redshift password: ")
    
    #Connect to Redshift
    try:
        conn = psycopg2.connect("host=" + str(host) + " port=" + str(port) + " dbname=" + str(dbname) + " user=" + str(username) + " password=" + str(password))
        conn.autocommit = True
    except:
        print("Cannot connect to Redshift")
        exit()
    
    return conn

#Get information for the specified units
def get_units(conn, unitlist):
    #Cursor to get unit list
    units = conn.cursor()
    
    units.execute("""select null as profileid,
                        displayname,
                        timezone,
                        cabinetserial,
                        case when invertermodel in ('schneider/XW4548-230-50','schneider/XW4548-120-240-60') then 4548
                            when invertermodel in ('schneider/XW6048-120-240-60','schneider/XW6048-230-50') then 6048
                            when invertermodel = 'schneider/XWPLUS6848-120-240-60' then 6848
                            when invertermodel = 'schneider/XWPLUS8548-230-50' then 8548
                        else null end as invertercapacity,
                        case when batterymodel = 'kokam/75AH-14S-2P' then 7780
                            when batterymodel = 'kokam/52AH-14S-4P' then 10770
                            when batterymodel = 'kokam/75AH-14S-3P' then 11640
                            when batterymodel = 'kokam/79AH-14S-3P' then 12300
                            when batterymodel = 'kokam/75AH-14S-5P' then 19400
                        else null end as batterycapacity,
                        pvarraysize * 1000.0 as pvcapacity,
                        null as day_offset,
                        null as hour_offset,
                        null as actual_start,
                        null as actual_end
                        from dmc_units
                    where displayname in %s
                    order by displayname;""", (unitlist,))
    
    return units

#Get information for the specified units
def get_profiles(conn, profilelist):
    #Cursor to get unit list
    profiles = conn.cursor()
    
    profiles.execute("""select profileid,
                        loadprofile,
                        timezone,
                        cabinetserial,
                        null as invertercapacity,
                        null as batterycapacity,
                        null as pv_capacity,
                        day_offset,
                        hour_offset,
                        actual_start,
                        actual_end
                    from profiles
                    where loadprofile in %s
                    order by loadprofile;""", (profilelist,))
    
    return profiles

def get_unit_high_res_data(conn, timezone, cabinetserial, actual_start_local, actual_end_local):
    high_res_cursor = conn.cursor()
    
    #Get high-res data for simulation
    high_res_cursor.execute("""select ddd.polldate
            ,CONVERT_TIMEZONE (%s, ddd.polldate) as local_timestamp
            ,status_any_red + status_no_red as duration_ms
            ,pv_power
            ,null as pv_power_1kW
            ,load_power
            ,main_load_power
            ,critical_load_power
        from dessdevicedata ddd
        join maggiedevicedata mdd on mdd.clientid = ddd.clientid and mdd.polldate = ddd.polldate
        join dmc_devices d on ddd.clientid = d.clientid
        where cabinetserial = %s
            and ddd.polldate >= CONVERT_TIMEZONE (%s, 'UTC', %s)
            and ddd.polldate < CONVERT_TIMEZONE (%s, 'UTC', %s)
            and mdd.polldate >= CONVERT_TIMEZONE (%s, 'UTC', %s)
            and mdd.polldate < CONVERT_TIMEZONE (%s, 'UTC', %s)
            and status_any_red is not null
            and status_no_red is not null
            and pv_power is not null
            and load_power is not null
            and main_load_power is not null
            and critical_load_power is not null
        order by ddd.polldate;""",[timezone,
                                   cabinetserial,
                                   timezone,
                                   actual_start_local,
                                   timezone,
                                   actual_end_local,
                                   timezone,
                                   actual_start_local,
                                   timezone,
                                   actual_end_local])
    
    return high_res_cursor


def get_profile_high_res_data(conn, day_offset, hour_offset, timezone, profile_id, cabinetserial, actual_start_utc, actual_end_utc, actual_start_local, actual_end_local):
    high_res_cursor = conn.cursor()
    
    #Get high-res data for simulation
    high_res_cursor.execute("""select * from
                        (select date_add('day', %s, date_add('hour', %s, ddd.polldate)) as polldate
                            ,CONVERT_TIMEZONE (%s, date_add('day', %s, date_add('hour', %s, ddd.polldate))) as local_timestamp
                            ,status_any_red + status_no_red as duration_ms
                            ,null as pv_power
                            ,pv_power * pv_power_ratio as pv_power_1kW
                            ,load_power * load_power_ratio as load_power
                            ,main_load_power * load_power_ratio as main_load_power
                            ,critical_load_power * load_power_ratio as critical_load_power
                        from dessdevicedata ddd
                        join maggiedevicedata mdd on mdd.clientid = ddd.clientid and mdd.polldate = ddd.polldate
                        join dmc_devices d on ddd.clientid = d.clientid
                        join profilecomposites pc on pc.profileid = %s and pc.actual_month = date_part('month', ddd.polldate)
                        where cabinetserial = %s
                            and ddd.polldate >= %s
                            and ddd.polldate < %s
                            and mdd.polldate >= %s
                            and mdd.polldate < %s
                            and status_any_red is not null
                            and status_no_red is not null
                            and pv_power is not null
                            and load_power is not null
                            and main_load_power is not null
                            and critical_load_power is not null)
                    where polldate >= CONVERT_TIMEZONE (%s, 'UTC', %s)
                        and polldate < CONVERT_TIMEZONE (%s, 'UTC', %s)
                    order by polldate;
                    """, [day_offset,
                            hour_offset,
                            timezone,
                            day_offset,
                            hour_offset,
                            profile_id,
                            cabinetserial,
                            actual_start_utc,
                            actual_end_utc,
                            actual_start_utc,
                            actual_end_utc,
                            timezone,
                            actual_start_local,
                            timezone,
                            actual_end_local])
    
    return high_res_cursor

def get_unit_historical_data(conn, timezone, cabinetserial, actual_start_local, actual_end_local):
    low_res_cursor = conn.cursor()
    
    #Get low-res historical data
    low_res_cursor.execute("""select date_trunc('hour', polldate) + date_part('minute', polldate)::int / 15 * interval '15 min' as utc_timestamp,
            trunc(polldate) as utc_date,
            date_part('hour', polldate) as utc_hour,
            15 * (date_part('minute', polldate)::int/15) as utc_15min,
            avg(load_power) as load_power,
            avg(pv_power) as pv_power,
            null as pv_power_1kW
          from dessdevicedata ddd
          join dmc_devices d on ddd.clientid = d.clientid
          where cabinetserial = %s
            and polldate >= CONVERT_TIMEZONE (%s, 'UTC', date_add('day', -30, %s))
            and polldate < CONVERT_TIMEZONE (%s, 'UTC', %s)
            and pv_power is not null
            and load_power is not null
          group by utc_timestamp, utc_date, utc_hour, utc_15min
          order by utc_timestamp, utc_date, utc_hour, utc_15min;""",[cabinetserial,
                                                                     timezone,
                                                                     actual_start_local,
                                                                     timezone,
                                                                     actual_end_local])

    historical = pd.DataFrame(columns = ['utc_timestamp', 'utc_date', 'utc_hour', 'utc_15min', 'load_power', 'pv_power', 'pv_power_1kW'])
    
    for datarow in low_res_cursor:
        historical_row = pd.DataFrame([[datarow[0], datarow[1], datarow[2], datarow[3], datarow[4], datarow[5], datarow[6]]], columns = ['utc_timestamp', 'utc_date', 'utc_hour', 'utc_15min', 'load_power', 'pv_power', 'pv_power_1kW'])
        historical = historical.append(historical_row, ignore_index=True)
    
    return historical
    

def get_profile_historical_data(conn, day_offset, hour_offset, profile_id, cabinetserial, actual_start_utc, actual_end_utc, timezone, actual_start_local, actual_end_local):
    low_res_cursor = conn.cursor()
    
    #Get low-res historical data
    low_res_cursor.execute("""select * from
                            (select date_trunc('hour', date_add('day', %s, date_add('hour', %s, ddd.polldate))) + date_part('minute', polldate)::int / 15 * interval '15 min' as utc_timestamp,
                              trunc(date_add('day', %s, date_add('hour', %s, ddd.polldate))) as utc_date,
                              date_part('hour', date_add('day', %s, date_add('hour', %s, ddd.polldate))) as utc_hour,
                              15 * (date_part('minute', date_add('day', %s, date_add('hour', %s, ddd.polldate)))::int/15) as utc_15min,
                              avg(load_power * load_power_ratio) as load_power,
                              null as pv_power,
                              avg(pv_power * pv_power_ratio) as pv_power_1kW
                            from dessdevicedata ddd
                            join dmc_devices d on ddd.clientid = d.clientid
                            join profilecomposites pc on pc.profileid = %s and pc.actual_month = date_part('month', ddd.polldate)
                            where cabinetserial = %s
                              and polldate >= date_add('day', -30, %s)
                              and polldate < %s
                              and pv_power is not null
                              and load_power is not null
                            group by utc_timestamp, utc_date, utc_hour, utc_15min)
                            where utc_timestamp >= CONVERT_TIMEZONE (%s, 'UTC', date_add('day', -30, %s))
                                and utc_timestamp < CONVERT_TIMEZONE (%s, 'UTC', %s)
                            order by utc_timestamp;""",[day_offset,
                                                        hour_offset,
                                                        day_offset,
                                                        hour_offset,
                                                        day_offset,
                                                        hour_offset,
                                                        day_offset,
                                                        hour_offset,
                                                        profile_id,
                                                        cabinetserial,
                                                        actual_start_utc,
                                                        actual_end_utc,
                                                        timezone,
                                                        actual_start_local,
                                                        timezone,
                                                        actual_end_local])
    
    historical = pd.DataFrame(columns = ['utc_timestamp', 'utc_date', 'utc_hour', 'utc_15min', 'load_power', 'pv_power', 'pv_power_1kW'])
    
    for datarow in low_res_cursor:
        historical_row = pd.DataFrame([[datarow[0], datarow[1], datarow[2], datarow[3], datarow[4], datarow[5], datarow[6]]], columns = ['utc_timestamp', 'utc_date', 'utc_hour', 'utc_15min', 'load_power', 'pv_power', 'pv_power_1kW'])
        historical = historical.append(historical_row, ignore_index=True)
    
    return historical


def parse_profile(profile):
    #These are used for both versions of the script
    name = profile[1]
    timezone = profile[2]
    pytz_timezone = pytz.timezone (timezone) #Create timezone object with local timezone
    cabinetserial = profile[3]
    
    #These are used for unit version only, null for profile version
    profile_id = profile[0]
    invertercapacity_actual = profile[4]
    batterycapacity_actual = profile[5]
    pvcapacity_actual = profile[6]
    
    #These are used for profile version only, null for unit version
    day_offset = profile[7]
    hour_offset = profile[8]
    actual_start_utc = profile[9]
    actual_end_utc = profile[10]
    
    return profile_id, name, timezone, pytz_timezone, cabinetserial, invertercapacity_actual, batterycapacity_actual, pvcapacity_actual, day_offset, hour_offset, actual_start_utc, actual_end_utc

def get_results_EA(startTime, scenario_id, high_res_cursor, historical, battery_capacity, inverter_capacity, pv_capacity, pytz_timezone, peakperiodlist, chargeperiodlist, net_energy_metering, critical_peak_period, charge_rate, allow_export, charge_limit, program_soc_min, program_soc_max, demand_charge_reduction):
    
    site_demand_target, inverter_ac_power, active_state, battery_power, grid_power, site_demand_power, battery_effective_soc, battery_stored_energy, program_capacity, program_stored_energy, SOC_buffer, charge_buffer, pv_power_aux_ac, battery_SOH, results, current_month, current_day, polldate_15min, site_in_power_log = initialize_variables(program_soc_min, program_soc_max, battery_capacity)
    
    historical = adjust_historical_EA(historical, pv_capacity)
    
    #Loop through rows
    for row in high_res_cursor:
        
        #Asign variables
        polldate, local_timestamp, duration_ms, pv_power_unconstrained, pv_power, load_power, main_load_power, critical_load_power, new_polldate_15min = parse_row(row, pv_capacity)
        
        #Is it a new month? (local time)
        #If so, print that we are starting a new month
        if current_month != local_timestamp.month:
            print('Month ' + str(local_timestamp.month) + ' begins at ' + str(datetime.datetime.now() - startTime))
            current_month = local_timestamp.month
        
        #Is it a new day? (local time)
        #If so, calculate historical load and PV profile for this unit
        if current_day != local_timestamp.day:
            current_day = local_timestamp.day
            load_profile_historical = get_daily_results_EA(historical, local_timestamp, polldate)
            
        #Is it a new 15-minute period? (local time)
        #If so, calculate site demand target and charge target for this 15-minute period
        if polldate_15min != new_polldate_15min:
            polldate_15min = new_polldate_15min
            site_demand_target_15min, charge_OK, charge_target_soc = get_15_min_results_EA(load_profile_historical, historical, polldate, local_timestamp, pytz_timezone, polldate_15min, peakperiodlist, chargeperiodlist, program_stored_energy, program_capacity, net_energy_metering, critical_peak_period, allow_export, battery_capacity, demand_charge_reduction, site_in_power_log)
        
        #Get active state for Energy Arbitrage
        active_state, charge_buffer, SOC_buffer, pv_power = get_active_state_EA(charge_buffer, SOC_buffer, allow_export, charge_OK, battery_effective_soc, program_soc_min, program_soc_max, pv_power, charge_target_soc)
        
        #Get active state for DCR
        current_period_type = peakflag(local_timestamp, peakperiodlist)
        active_state, charge_buffer, SOC_buffer, pv_power = get_active_state_DCR(charge_buffer, SOC_buffer, allow_export, charge_OK, battery_effective_soc, program_soc_min, program_soc_max, pv_power, charge_target_soc, current_period_type)
        
        #Calculate poll variables for selected mode
        site_demand_target, inverter_ac_power, battery_power, grid_power, site_demand_power, battery_stored_energy, battery_effective_soc, net_load_power, program_stored_energy = get_poll_results(active_state, site_demand_target_15min, charge_rate, inverter_ac_power, pv_power, charge_limit, load_power, main_load_power, critical_load_power, battery_effective_soc, inverter_capacity, duration_ms, battery_SOH, battery_capacity, program_soc_min, battery_stored_energy)

        current_results = [scenario_id,
                           polldate,
                           duration_ms,
                           pv_power,
                           load_power,
                           main_load_power,
                           critical_load_power,
                           pv_power_unconstrained,
                           pv_power_aux_ac,
                           active_state,
                           grid_power,
                           site_demand_power,
                           battery_power,
                           battery_stored_energy,
                           battery_effective_soc,
                           battery_SOH,
                           net_load_power,
                           inverter_ac_power,
                           site_demand_target]
        
        results.append(current_results)
        
        local_billing_period = local_timestamp - datetime.timedelta(minutes=local_timestamp.minute % 30,
                             seconds=local_timestamp.second,
                             microseconds=local_timestamp.microsecond)
        
        site_in_power = max(site_demand_power, 0)
        
        site_in_power_log_row = pd.DataFrame([[polldate, local_billing_period, peakflag(local_timestamp, peakperiodlist), site_in_power]], columns = ['polldate', 'local_billing_period', 'peak_type', 'site_in_power'])
        site_in_power_log = site_in_power_log.append(site_in_power_log_row, ignore_index=True)
        
    return results


def initialize_variables(program_soc_min, program_soc_max, battery_capacity):
    #Set battery to 20% SOC, all power readings to zero
    site_demand_target = 0
    inverter_ac_power = 0 
    active_state = "STANDBY"
    battery_power = 0
    grid_power = 0
    site_demand_power = 0
    battery_effective_soc = 20
    battery_stored_energy = (battery_effective_soc/100.0) * battery_capacity

    program_capacity = ((program_soc_max - program_soc_min)/100.0) * battery_capacity
    program_stored_energy = battery_stored_energy - ((program_soc_min/100.0) * battery_capacity)
    
    SOC_buffer = 0
    charge_buffer = 0
    
    #these variables are not used for this program
    pv_power_aux_ac = 0
    battery_SOH = 100
    
    #Initialize script variables
    results = []
    current_month = -1
    current_day = -1
    polldate_15min = -1
    
    site_in_power_log = pd.DataFrame(columns = ['polldate', 'local_billing_period', 'peak_type', 'site_in_power'])
    
    return site_demand_target, inverter_ac_power, active_state, battery_power, grid_power, site_demand_power, battery_effective_soc, battery_stored_energy, program_capacity, program_stored_energy, SOC_buffer, charge_buffer, pv_power_aux_ac, battery_SOH, results, current_month, current_day, polldate_15min, site_in_power_log


def parse_row(row, pv_capacity):
        polldate = row[0]
        local_timestamp = row[1]
        duration_ms = row[2]
        
        if row[4] is None:
            pv_power_unconstrained = row[3]
        else:
            #If using a profile, PV profiles are at 1kW, so scale by PV capacity
            pv_power_unconstrained = min(pv_capacity, row[4] * pv_capacity/float(1000))
            
        pv_power = min(7000, pv_power_unconstrained) #Limit PV to MPPT capacity
        load_power = row[5]
        main_load_power = row[6]
        critical_load_power = row[7]
        
        new_polldate_15min = polldate - datetime.timedelta(minutes=polldate.minute % 15, seconds=polldate.second, microseconds=polldate.microsecond)
        
        return polldate, local_timestamp, duration_ms, pv_power_unconstrained, pv_power, load_power, main_load_power, critical_load_power, new_polldate_15min

def adjust_historical_EA(historical, pv_capacity):
    #If using a profile and energy arbitrage, update low-res data based on PV capacity
    if historical['pv_power_1kW'].dtype == "float64":
        #PV profiles are at 1kW, so scale by PV capacity
        historical['pv_power'] = historical['pv_power_1kW'] * pv_capacity/float(1000)
 
        #Limit to SCC capacity
        historical['scc_limit'] = 7000
        historical['pv_power'] = historical[["pv_power", "scc_limit"]].min(axis=1)
        historical = historical.drop('scc_limit', 1)
                                     
    return historical
    

def get_daily_results_EA(historical, local_timestamp, new_polldate_15min):
    print("begin local day start calculations for " + local_timestamp.strftime("%Y-%m-%d %H:%M:%S"))
    
    #Get last 30 days of data
    historical_30day = historical[(historical['utc_timestamp'] < new_polldate_15min) & (historical['utc_timestamp'] >= new_polldate_15min - datetime.timedelta(days=30))]
    
    #Create load profile from last 30 days of data
    load_profile_historical = historical_30day.groupby(['utc_hour', 'utc_15min'], as_index=False).mean()

    return load_profile_historical


def get_15_min_results_EA(load_profile_historical, historical, polldate, local_timestamp, pytz_timezone, polldate_15min, peakperiodlist, chargeperiodlist, program_stored_energy, program_capacity, net_energy_metering, critical_peak_period, allow_export, battery_capacity, demand_charge_reduction, site_in_power_log):

    if demand_charge_reduction == 1:
        #For DCR, set lookback period to x hours
        temp_utc_timestamp = polldate_15min - datetime.timedelta(hours=2)
    else:
        #Step 1 - Actual vs. Expected Load and PV ratios
        #Calculate load_ratio_1day table
        temp_local_timestamp = local_timestamp - datetime.timedelta(minutes=15) #Subtract 15 mins # Unmesh : What is the purppose of doing this? I believe it is adjustments around midnight
        temp_local_timestamp = temp_local_timestamp.replace(hour=0, minute=0, second=0, microsecond=0) #Round to start of day
         
        temp_local_timestamp = pytz_timezone.localize(temp_local_timestamp, is_dst=None) #Apply timezone to local date
        temp_utc_timestamp = temp_local_timestamp.astimezone (pytz.utc) #Convert local date to UTC
    
    load_ratio_1day = historical[(historical['utc_timestamp'] < polldate_15min) & (historical['utc_timestamp'] >= temp_utc_timestamp)]
    
    temp_local_timestamp = None
    temp_utc_timestamp = None
    
    load_ratio_1day = load_ratio_1day.reset_index(drop=True)
    
    load_ratio_1day = load_ratio_1day[['utc_hour', 'utc_15min', 'load_power', 'pv_power']]
    
    load_ratio_1day.columns = ['utc_hour', 'utc_15min', 'load_power_actual', 'pv_power_actual']
    
    #Combine day-to-date and historical profiles
    load_ratio_combined = pd.merge(load_profile_historical, load_ratio_1day, on=['utc_hour', 'utc_15min'])
    
    load_ratio_combined_avg = load_ratio_combined.mean(axis=0)
    
    if load_ratio_combined_avg['load_power'] == 0:
        load_power_ratio_actual_expected = 1
    else:
        load_power_ratio_actual_expected = load_ratio_combined_avg['load_power_actual'] / load_ratio_combined_avg['load_power']
        
    if load_ratio_combined_avg['pv_power'] == 0:
        pv_power_ratio_actual_expected = 1
    else:
        pv_power_ratio_actual_expected = load_ratio_combined_avg['pv_power_actual'] / load_ratio_combined_avg['pv_power']
        
    
    #Step 2a - Create table and populate with Load and PV prediction
    #Calculate load_profile_1day table
    polldate_current_day = polldate.replace(hour=0, minute=0, second=0, microsecond=0)
    
    date_stage = pd.DataFrame(columns = ['utc_date'], data = [polldate_current_day, polldate_current_day + datetime.timedelta(days=1), polldate_current_day + datetime.timedelta(days=2)])
    
    load_profile_historical['key'] = 0
    date_stage['key'] = 0
    
    load_profile_1day = load_profile_historical.merge(date_stage, how='left', on = 'key')

    load_profile_1day['load_power_prediction'] = load_profile_1day['load_power'] * load_power_ratio_actual_expected
    load_profile_1day['pv_power_prediction'] = load_profile_1day['pv_power'] * pv_power_ratio_actual_expected
        
    load_profile_1day['utc_timestamp'] = None
    
    load_profile_1day['local_timestamp'] = None
    
    load_profile_1day['peak_type'] = None
    
    load_profile_1day['peak_id'] = None
    
    for i, row in load_profile_1day.iterrows():
        temp_utc_timestamp = row.utc_date + datetime.timedelta(hours=row.utc_hour) + datetime.timedelta(minutes=row.utc_15min)
        load_profile_1day.set_value(i,'utc_timestamp', temp_utc_timestamp)
        
        #temp_utc_timestamp = pytz.utc.localize(temp_utc_timestamp, is_dst=None) #Apply timezone to UTC date
        temp_local_timestamp = temp_utc_timestamp.astimezone (pytz_timezone)
        load_profile_1day.set_value(i,'local_timestamp', temp_local_timestamp)
        
        load_profile_1day.set_value(i,'peak_type', peakflag(temp_local_timestamp, peakperiodlist))
    
    load_profile_1day = load_profile_1day.sort_values(['utc_timestamp'])
    
    polldate_trunc = polldate - datetime.timedelta(minutes=polldate.minute % 10, seconds=polldate.second, microseconds=polldate.microsecond)
    
    load_profile_1day = load_profile_1day.loc[(load_profile_1day["utc_timestamp"] >= polldate_trunc) & (load_profile_1day["utc_timestamp"] < polldate_trunc + datetime.timedelta(days=2)), ['utc_timestamp', 'local_timestamp', 'peak_type', 'load_power_prediction', 'pv_power_prediction']]
    
    load_profile_1day = load_profile_1day.reset_index(drop=True)
    
    prev = None
    peak_id = 0
    #Flag peaks 
    for i, cur in load_profile_1day.iterrows():
        if prev is None:
            load_profile_1day.set_value(i,'peak_id', peak_id)
        else:
            if cur.peak_type != prev.peak_type:
                peak_id += 1
            load_profile_1day.set_value(i,'peak_id', peak_id)
        prev = cur
    
    #Step 2b - Set Net Load prediction
    load_profile_1day['net_load_prediction'] = load_profile_1day['load_power_prediction'] - load_profile_1day['pv_power_prediction']
    
    #Step 2c - Set Predicted Program Stored Energy
    load_profile_1day['program_stored_energy_prediction'] = program_stored_energy - load_profile_1day.net_load_prediction.cumsum()/float(4)
    
    load_profile_1day['program_stored_energy_target'] = None
    
    load_profile_1day['period_flag_start'] = None
    
    load_profile_1day['period_flag_end'] = None
    
    load_profile_1day['min_target'] = None
    
    load_profile_1day['max_target'] = None
    
    prev = None
    #Flag periods
    for i, cur in load_profile_1day.iterrows():
        if prev is None:
            load_profile_1day.set_value(i,'period_flag_start', 0)
        else:
            if (np.sign(cur.net_load_prediction) != np.sign(prev.net_load_prediction) and (prev.program_stored_energy_prediction < (0.03 * program_capacity) or prev.program_stored_energy_prediction > (0.97 * program_capacity))) or cur.peak_type != prev.peak_type:
                load_profile_1day.set_value(i,'period_flag_start', 1)
                load_profile_1day.set_value(i-1,'period_flag_end', 1)
            else:
                load_profile_1day.set_value(i,'period_flag_start', 0)
                load_profile_1day.set_value(i-1,'period_flag_end', 0)
        prev = cur
    
    load_profile_1day = load_profile_1day.sort_values(['utc_timestamp'], ascending = False)
     
    prev_program_stored_energy_target = None
    prev = None
    #Calculate program_stored_energy_target
    for i, cur in load_profile_1day.iterrows():
        if prev is None:
            current_program_stored_energy_target = 0.03 * program_capacity
        elif prev.peak_type == 0:
            current_program_stored_energy_target = 0.03 * program_capacity
        else:
            current_program_stored_energy_target = max(min((cur.net_load_prediction/float(4)) + prev_program_stored_energy_target, 0.97 * program_capacity), 0.03 * program_capacity)
        load_profile_1day.set_value(i,'program_stored_energy_target', current_program_stored_energy_target)
        prev_program_stored_energy_target = current_program_stored_energy_target
        prev = cur
     
    load_profile_1day = load_profile_1day.sort_values(['utc_timestamp'])
    
    #Set last row
    load_profile_1day.iloc[-1, load_profile_1day.columns.get_loc('period_flag_end')] = 1
        
    prev = None
    #Set targets
    for i, cur in load_profile_1day.iterrows():
        if prev is not None:
            if cur.period_flag_start == 1:
                if cur.peak_type == prev.peak_type: #No peak change
                    load_profile_1day.set_value(i-1,'min_target', 0.03 * program_capacity)
                    load_profile_1day.set_value(i-1,'max_target', 0.97 * program_capacity)
                if cur.peak_type > prev.peak_type: #We are moving into a higher value peak period
                    if net_energy_metering == 1 or critical_peak_period == 1:
                        load_profile_1day.set_value(i-1,'min_target', 0.97 * program_capacity)
                        load_profile_1day.set_value(i-1,'max_target', 0.97 * program_capacity)
                    else:
                        load_profile_1day.set_value(i-1,'min_target', cur.program_stored_energy_target)
                        load_profile_1day.set_value(i-1,'max_target', cur.program_stored_energy_target)
                if cur.peak_type < prev.peak_type: #We are moving into a lower value peak period
                    load_profile_1day.set_value(i-1,'min_target', 0.03 * program_capacity)
                    if demand_charge_reduction == 1:
                        load_profile_1day.set_value(i-1,'max_target', 0.97 * program_capacity) #For DCR max target is 97% as we're not trying to empty the battery during the peak
                    else:
                        load_profile_1day.set_value(i-1,'max_target', 0.03 * program_capacity)
        prev = cur
    

    #Set last row
    if load_profile_1day['min_target'].iloc[-1] is None:
        load_profile_1day.iloc[-1, load_profile_1day.columns.get_loc('min_target')] = 0.03 * program_capacity
    if load_profile_1day['max_target'].iloc[-1] is None:
        load_profile_1day.iloc[-1, load_profile_1day.columns.get_loc('max_target')] = 0.97 * program_capacity
        
    load_profile_1day['period_id'] = load_profile_1day.period_flag_start.cumsum()
    
    first_period = load_profile_1day[load_profile_1day['period_id'] == 0]
    
    duration_hrs = len(first_period)/float(4)
    
    actual_ending_stored_energy = first_period['program_stored_energy_prediction'].iloc[-1]
    
    if first_period['program_stored_energy_prediction'].iloc[-1] < first_period['min_target'].iloc[-1]:
        target_ending_stored_energy = first_period['min_target'].iloc[-1]
    elif first_period['program_stored_energy_prediction'].iloc[-1] > first_period['max_target'].iloc[-1]:
        target_ending_stored_energy = first_period['max_target'].iloc[-1]
    else:
        target_ending_stored_energy = first_period['program_stored_energy_prediction'].iloc[-1]
        
    starting_program_stored_energy_prediction = first_period['program_stored_energy_prediction'].iloc[0]
    
    site_demand_target_15min = ((target_ending_stored_energy - program_stored_energy) - (actual_ending_stored_energy - program_stored_energy))/float(duration_hrs)
    site_demand_target_15min_min = ((0.03 * program_capacity) - starting_program_stored_energy_prediction) * 4.0
    site_demand_target_15min_max = ((0.97 * program_capacity) - starting_program_stored_energy_prediction) * 4.0
    
    site_demand_target_15min = min(max(site_demand_target_15min_min, site_demand_target_15min), site_demand_target_15min_max)
    
    #Check to catch calculation failure
    if np.isnan(site_demand_target_15min):
        print("WARNING: Site Demand Target calculation failure!")
        site_demand_target_15min = 0
    
    print("Site Demand Target calculation for " + local_timestamp.strftime("%Y-%m-%d %H:%M:%S") + " = " + str(site_demand_target_15min))

    if demand_charge_reduction == 1 and peakflag(local_timestamp, peakperiodlist) != 0:
        #Filter data to current local year and month
        current_month_site_in_power_log = site_in_power_log[(site_in_power_log['polldate'] < polldate_15min) & (pd.DatetimeIndex(site_in_power_log['local_billing_period']).year == local_timestamp.year) & (pd.DatetimeIndex(site_in_power_log['local_billing_period']).month == local_timestamp.month) & (site_in_power_log['peak_type'] == peakflag(local_timestamp, peakperiodlist))]
        
        #Average to nearest 15 mins
        current_month_site_in_power_log = current_month_site_in_power_log.groupby(['local_billing_period', 'peak_type'], as_index=False).mean()
        
        if not current_month_site_in_power_log.empty:
            max_demand = current_month_site_in_power_log['site_in_power'].values.max()
            
            if site_demand_target_15min < max_demand:
                site_demand_target_15min = max_demand
                print("Site Demand Target adjusted to " + str(site_demand_target_15min) + " due to previous monthly max demand")

    if net_energy_metering == 0 and site_demand_target_15min < 0:
        site_demand_target_15min = 0
        print("Site Demand Target adjusted to " + str(site_demand_target_15min) + " due to net energy metering")
    
    #Can we charge from grid? If so, calculate target
    if chargecheck(local_timestamp, chargeperiodlist) is True:
        charge_OK = True
        if demand_charge_reduction == 1:
            first_peak = load_profile_1day[load_profile_1day['peak_id'] == 0]
            first_peak_min_target = first_peak['min_target'].iloc[-1]
            charge_target_soc = min(max(100 * (first_peak_min_target / battery_capacity), 0), 100)
            print("Charge Target calculation for " + local_timestamp.strftime("%Y-%m-%d %H:%M:%S") + " = " + str(charge_target_soc))
        else:
            first_peak = load_profile_1day[load_profile_1day['peak_id'] == 0]
            first_peak_min_target = first_peak['min_target'].iloc[-1]
            first_peak_sum = first_peak.sum(axis=0)
            first_peak_pv_power = first_peak_sum['pv_power_prediction']/float(4)
            charge_target_soc = min(max(100 * ((first_peak_min_target - first_peak_pv_power) / battery_capacity), 0), 100)
            print("Charge Target calculation for " + local_timestamp.strftime("%Y-%m-%d %H:%M:%S") + " = " + str(charge_target_soc))
    else:
        charge_OK = False
        charge_target_soc = 0
    
    return site_demand_target_15min, charge_OK, charge_target_soc


def get_active_state_EA(charge_buffer, SOC_buffer, allow_export, charge_OK, battery_effective_soc, program_soc_min, program_soc_max, pv_power, charge_target_soc):
    #Determine active state for this poll
    #If SOC is high, only dispatch mode is available
    if battery_effective_soc > (program_soc_max - SOC_buffer):
        charge_buffer = 0
        SOC_buffer = 3
        if allow_export == 1: #If we are allowed to export, dispatch by power at inverter capacity
            active_state = "DISPATCH_POWER_BY_PV"
        else: #If we are not allowed to export, disabled PV. We don't need to limit the setpoint because the battery won't charge with PV disabled.
            active_state = "DISPATCH_POWER_BY_SITE_DEMAND"
            pv_power = 0
    
    elif charge_OK is True and battery_effective_soc < (program_soc_min + charge_target_soc + charge_buffer):
        charge_buffer = 3
        SOC_buffer = 0
        active_state = "CHARGE_FROM_GRID"
    
    elif battery_effective_soc < (program_soc_min + SOC_buffer):
        charge_buffer = 0
        SOC_buffer = 3
        active_state = "CHARGE_FROM_PV"
    
    #If SOC is normal, dispatch power by site demand
    else:
        charge_buffer = 0
        SOC_buffer = 0
        active_state = "DISPATCH_POWER_BY_SITE_DEMAND"
        
    return active_state, charge_buffer, SOC_buffer, pv_power


def get_active_state_DCR(charge_buffer, SOC_buffer, allow_export, charge_OK, battery_effective_soc, program_soc_min, program_soc_max, pv_power, charge_target_soc, current_period_type):
    #Determine active state for this poll
    #If SOC is high, only dispatch mode is available
    if charge_OK is True:
        if battery_effective_soc < (program_soc_min + charge_target_soc + charge_buffer): #We need more charge in the battery
            charge_buffer = 3
            SOC_buffer = 0
            active_state = "CHARGE_FROM_GRID"
        
        else: #We have enough charge
            charge_buffer = 0
            SOC_buffer = 0
            active_state = "CHARGE_FROM_PV"
            if allow_export == 0:
                pv_power = 0
                
    elif current_period_type == 0: #We are in an off-peak period
        if battery_effective_soc > (program_soc_max - SOC_buffer): 
            charge_buffer = 0
            SOC_buffer = 3
            active_state = "DISPATCH_POWER_BY_PV"
            if allow_export == 0:
                pv_power = 0

        else:
            charge_buffer = 0
            SOC_buffer = 0
            active_state = "CHARGE_FROM_PV"
            
    else: #We are in a peak or shoulder period
        if battery_effective_soc > (program_soc_max - SOC_buffer): 
            charge_buffer = 0
            SOC_buffer = 3
            if allow_export == 1: #If we are allowed to export, dispatch by power at inverter capacity
                active_state = "DISPATCH_POWER_BY_POWER"
            else: #If we are not allowed to export, disabled PV. We don't need to limit the setpoint because the battery won't charge with PV disabled.
                active_state = "DISPATCH_POWER_BY_SITE_DEMAND"
                pv_power = 0
        
        elif battery_effective_soc < (program_soc_min + SOC_buffer):
            charge_buffer = 0
            SOC_buffer = 3
            active_state = "CHARGE_FROM_PV"
    
        #If SOC is normal, dispatch power by site demand
        else:
            charge_buffer = 0
            SOC_buffer = 0
            active_state = "DISPATCH_POWER_BY_SITE_DEMAND"
            
    return active_state, charge_buffer, SOC_buffer, pv_power


def get_poll_results(active_state, site_demand_target_15min, charge_rate, inverter_ac_power, pv_power, charge_limit, load_power, main_load_power, critical_load_power, battery_effective_soc, inverter_capacity, duration_ms, battery_SOH, battery_capacity, program_soc_min, battery_stored_energy):
    #Calculate variable changes due to current state 
    if active_state == "CHARGE_FROM_PV":
        site_demand_target = 0 #This is only used for dispatch by site demand
        inverter_ac_power = 0 #This is only used for dispatch modes
        if battery_effective_soc <= 1: #battery is completely drained
            battery_power = min(pv_power * pv_to_batt_eff(pv_power),charge_limit)
        else:
            battery_power = min(pv_power * pv_to_batt_eff(pv_power),charge_limit) -32 #Send PV power to battery with 93% efficiency, minus 32 watts parasitic loss. Any PV greater than charge limit is lost.
        grid_power = critical_load_power / grid_to_load_eff(critical_load_power) #Support critical load from grid, with 91% grid to load efficiency
        site_demand_power = grid_power + main_load_power #Site demand power = grid power + main load
        
    elif active_state == "CHARGE_FROM_GRID":
        site_demand_target = 0 #This is only used for dispatch by site demand
        inverter_ac_power = 0 #This is only used for dispatch modes
        charge_rate = min(charge_rate,inverter_capacity,charge_limit) #limit charge_rate to inverter_capacity and charge limit
        battery_power = (charge_rate * grid_to_batt_eff(charge_rate)) -32 #Charge battery at charge_rate watts, 90% grid to battery efficiency, minus 32 watts parasitic loss
        grid_power = charge_rate + (critical_load_power / grid_to_load_eff(critical_load_power)) #Support critical load from grid, with 91% grid to load efficiency; also charge battery from grid
        site_demand_power = grid_power + main_load_power #Site demand power = grid power + main load
        
    elif active_state == "DISPATCH_POWER_BY_SITE_DEMAND":
        #Limit battery power to charge limit, and find new inverter ac power
        site_demand_target = site_demand_target_15min
        inverter_ac_power = max(
                                min(inverter_ac_power/grid_to_batt_eff(abs(inverter_ac_power)), pv_power + charge_limit),
                                pv_power - charge_limit
                                )
        inverter_ac_power = inverter_ac_power * grid_to_batt_eff(abs(inverter_ac_power))
        battery_power = -((inverter_ac_power/grid_to_batt_eff(abs(inverter_ac_power))) - pv_power) -32 #Send to grid at inverter AC power, from PV and battery. 90% grid to battery efficiency, 32 watts parasitic loss
        grid_power = (critical_load_power / grid_to_load_eff(critical_load_power)) - inverter_ac_power #Grid power is difference between critical load (at 91% efficiency) and inverted power
        site_demand_power = grid_power + main_load_power#Site demand power = grid power + main load
        
        #Determine if we should increase or decrease inverter_ac_power for next time
        if site_demand_power < (site_demand_target - 300) or site_demand_power > (site_demand_target + 300):
            inverter_ac_power = max(load_power - site_demand_target, 0)
        
    elif active_state == "DISPATCH_POWER_BY_PV":
        site_demand_target = 0 #This is only used for dispatch by site demand
        inverter_ac_power = pv_power + 400 #Invert all PV plus an additional 400 watts. The additional 400 watts ensures that we are always discharging the battery.

        #Limit battery power to charge limit, and find new inverter ac power
        inverter_ac_power = max(
                                min(inverter_ac_power/grid_to_batt_eff(abs(inverter_ac_power)), pv_power + charge_limit),
                                pv_power - charge_limit
                                )
        inverter_ac_power = inverter_ac_power * grid_to_batt_eff(abs(inverter_ac_power))

        battery_power = -((inverter_ac_power/grid_to_batt_eff(abs(inverter_ac_power))) - pv_power) -32 #Send to grid at inverter AC power, from PV and battery. 90% grid to battery efficiency, 32 watts parasitic loss
        
        grid_power = (critical_load_power / grid_to_load_eff(critical_load_power)) - inverter_ac_power #Grid power is difference between critical load (at 91% efficiency) and inverted power
        site_demand_power = grid_power + main_load_power #Site demand power = grid power + main load
        
    elif active_state == "DISPATCH_POWER_BY_POWER":
        site_demand_target = 0 #This is only used for dispatch by site demand
        inverter_ac_power = inverter_capacity #Invert at inverter capacity

        inverter_ac_power = max(
                                min(inverter_ac_power,inverter_capacity, charge_limit + pv_power * pv_to_grid_eff(pv_power)),
                            -inverter_capacity, -charge_limit - pv_power * pv_to_grid_eff(pv_power)) #limit inverter_ac_power to inverter capacity
        battery_power = -((inverter_ac_power/grid_to_batt_eff(abs(inverter_ac_power))) - pv_power) -32 #Send to grid at inverter AC power, from PV and battery. 90% grid to battery efficiency, 32 watts parasitic loss
        grid_power = (critical_load_power / grid_to_load_eff(critical_load_power)) - inverter_ac_power #Grid power is difference between critical load (at 91% efficiency) and inverted power
        site_demand_power = grid_power + main_load_power #Site demand power = grid power + main load
    else:
        print("Error: active_state not recognised!")
        exit()

    #Calculate remaining variables
    battery_stored_energy += (battery_power * duration_ms / (1000 * 60 * 60.0))
    battery_effective_soc = max(battery_SOH * battery_stored_energy / float(battery_capacity),0)
    net_load_power = load_power - pv_power * pv_to_grid_eff(pv_power)
    program_stored_energy = battery_stored_energy - ((program_soc_min/100.0) * battery_capacity)
        
    return site_demand_target, inverter_ac_power, battery_power, grid_power, site_demand_power, battery_stored_energy, battery_effective_soc, net_load_power, program_stored_energy


def results_to_csv(startTime, results, csv_filename):
    print('csv write begins at ' + str(datetime.datetime.now() - startTime))
    
    with open(csv_filename, 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(results)
    
    f.close()

def csv_to_redshift(startTime, conn, csv_filename, s3_bucket_name, aws_access_key_id, aws_secret_access_key, scenario_id, scenario, profile_id, inverter_capacity, battery_capacity, pv_capacity, cabinetserial):
    #Check we have all the variable information, and request input from user if necessary
    if s3_bucket_name is None:
        s3_bucket_name = raw_input("Enter s3 bucket name: ")
    
    if aws_access_key_id is None:
        aws_access_key_id = raw_input("Enter AWS Access Key ID: ")
    
    if aws_secret_access_key is None:
        aws_secret_access_key = raw_input("Enter AWS Secret Access Key: ")
    
    #Compress file to gzip
    with open(csv_filename) as f_in, gzip.open(csv_filename + '.gz', 'wb') as f_out:
        f_out.writelines(f_in)

    #connect to s3 bucket
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(s3_bucket_name)
    
    print('Redshift csv.gz upload begins at ' + str(datetime.datetime.now() - startTime))
    
    #Upload gzip file to Redshift
    bucket.upload_file(csv_filename + '.gz', csv_filename + '.gz')
            
    print('Redshift COPY statment begins at ' + str(datetime.datetime.now() - startTime))
    
    copy = conn.cursor()
    
    #first, add a scenario log for this data 
    copy.execute("""insert into scenarios values (%s, %s, %s, %s, %s, %s, %s, %s);""",
                 [scenario_id, scenario, datetime.datetime.now(), profile_id, inverter_capacity, battery_capacity, pv_capacity, cabinetserial])
    
    #now, execute copy statement
    copy.execute("""copy simulations from 's3://""" + s3_bucket_name + """/""" + csv_filename + """.gz' 
        credentials 'aws_access_key_id=""" + aws_access_key_id + """;aws_secret_access_key=""" + aws_secret_access_key + """' 
        delimiter ',' gzip;""")
    
    # Make the changes to the database persistent
    conn.commit()