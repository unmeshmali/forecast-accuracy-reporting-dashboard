
import pandas as pd
import numpy as np
import psycopg2
from datetime import datetime as dt


df = pd.read_csv('/Users/unmeshmali/Desktop/Unmesh/Dash/forecastDash/database/tenantnames.csv')

def make_dropdown_list(dataFrame):
	tenantChoice = []
	for i in dataFrame.tenantname.unique():
		op = {'label': i, 'value': i}
		tenantChoice.append(op)
	return tenantChoice

def make_displayname_drop_list(dataFrame, tenantName):
	displayChoice = []
	df_sub = dataFrame[dataFrame.tenantname == tenantName]
	for j in df_sub.displayname:
		dp = {'label': j, 'value' : j}
		displayChoice.append(dp)
	return displayChoice



#make_dropdown_list(df)