# @Created on : 03/30/2020 11:40 am
# Author : Unmesh Mali
  
import dash
import plotly
import dash_core_components as dcc
import dash_html_components as html 
import dash_table
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc

from app import app
from tabs import sidepanel, loadTab, pvTab  
from database import getData
import sqlite3
import dash
from dash.dependencies import Input, Output, State
import dash_table
import pandas as pd

from app import app
from tabs import sidepanel, loadTab, pvTab
from database import getData

import gzip
from gzip import GzipFile
import boto3
import datetime 
from datetime import datetime as dt
import numpy as np
import re
import plotly.offline as pyo
import plotly.graph_objs as go

import sissim2


app.layout = sidepanel.layout

tz = 'Asia/Tokyo'


df = pd.read_csv('/Users/unmeshmali/Desktop/Unmesh/Dash/forecastDash/database/names.csv')


theme =  {
    'dark': True,
    'detail': '#007439',
    'primary': '#00EA64',
    'secondary': '#6E6E6E', 
}

columns = ['time', 'load', 'pv', 'dischargeCapacity', 'chargeCapacity',
       'maxDischargeRate', 'maxChargeRate']

Data = np.array([np.arange(192)]*7).T
forecast_df = pd.DataFrame(Data, columns = columns) 


# ------------------------------------------------------------------------------
# This block includes all the callback functions

"""
A callback to show the corresponding layout of the selected tab
"""

@app.callback(Output('my-client', 'options'),
              [Input('my-tenant', 'value')])    

def make_displayname_drop_list(tenantName):
    displayChoice = []
    df_sub = df[df.tenantname == tenantName]
    for j in df_sub.displayname:
        dp = {'label': j, 'value' : j}
        displayChoice.append(dp)
    return displayChoice


"""
A callback function to get Load and PV charts
"""

@app.callback(Output('tabs-content', 'figure'),
                [Input('update-inputs', 'n_clicks')],
                state = [State('my-client', 'value'),
                State('my-date', 'date'),
                State('my-time', 'value'),
                State('hours-ahead', 'value'),
                State('tabs', 'value')],
                )

def make_load_chart(n_clicks, Client, Date, Time, HoursAhead, tab):
    traces = [] # To be later used to store data in a dictionary.

    if n_clicks:        
        client = boto3.client('s3')
        resource = boto3.resource('s3')
        my_bucket = resource.Bucket('demo-sunverge-forecast-power-energy')
        Clientid = []
        dff = forecast_df
        forecast_dff = forecast_df
        
        # Temporarily I am hard coding the clientid to certain displaynames because Kyushu tenant is on the demo server
        if Client == 'Tokyo Gas 1':
            Clientid.append("24d1bcb3-8857-4341-beac-04aa97941787") # Mr. Ishibashi house
        else : 
            Clientid.append("5d995db9-8761-4379-ba0f-a8373184a1db") # Intelligent House   
     
        #Time = "%02d"%Time   

        # Update on : June 18th, 2020 @2:40pm
        # Need to replace this with a loop that iterates through all the files in Amazon S3 using the 'hoursAhead' clause
        # The major challenge is that Dash doesn't have a datetime input component. 
        # So I have to take date and time as separate inputs and then stictch them together like I have been doing so far. No way around it  man. 
        lists = []
        for i in range(90):
            time_offset = i*15
            date = dt.strptime(re.split('T| ', str(Date))[0], '%Y-%m-%d')
            date += datetime.timedelta(hours = Time)
            date += datetime.timedelta(minutes = time_offset)
            Key_Date = date.strftime("%Y-%m-%d" + 'T' + "%H:%M:%S")
            Key = ("clientid=" + '"' + str(Clientid[0]) + '"' + "/createdtime=" + str(Key_Date) + "/power-energy-forecast-" + str(Key_Date) + 'Z.csv.gz')
            obj = client.get_object(Bucket = 'demo-sunverge-forecast-power-energy', Key = Key)
            with gzip.open(obj['Body'], mode = 'rt') as f:
                forecast_dff = pd.read_csv(f)
            lists.append(forecast_dff.iloc[4*HoursAhead - 1])        
        #dff = pd.DataFrame(dff)    
        dff = pd.DataFrame(lists)

        # Convert the time column to datetime   
        dff['time'] = pd.to_datetime(dff['time'])       
        #Converting timezone to local time. The local time is defined by the vaiable tz. It is declared at the top. The time column is tz aware and hence used the tz_convert method  
        dff['time'] = dff['time'].dt.tz_convert(tz)
        dff['pv'] = dff['pv']*1000
        dff['load'] = dff['load']*1000
        
        dff.set_index('time', inplace = True)
        
        
        # Function to get the actual data from the database. Making sure that it is 15min averaged. 
        host = 'prod-sunverge-redshift.cvfma8pk1vjx.us-east-1.redshift.amazonaws.com'
        port = 5439
        dbname = 'datamart'
        username = 'jwang8'
        password = 'cyWang880'
        
        conn = sissim2.redshift_connect(host, port, dbname, username, password)
        Date = datetime.datetime.strptime(Date, "%Y-%m-%d").date()
        start_date = str(Date) + " " + str(Time) + ":00:00"
        
        end_date = str(Date + datetime.timedelta(days = 2)) + " " + str(Time) + ":00:00"
        tenantname = 'consumersenergy'
        displayname = 'CE_ES001_S.Devries'

        results_df = []
        def get_actual_data(conn, start_date, tenantname, end_date, displayname):
            # Start the connection and give it a name
            low_res_cursor = conn.cursor()
            
            low_res_cursor.execute("""select date_trunc('hour', polldate) + date_part('minute', polldate)::int / 15 * interval '15 min' as utc_timestamp,
            trunc(polldate) as utc_date,
            date_part('hour', polldate) as utc_hour,
            15 * (date_part('minute', polldate)::int/15) as utc_15min,
            avg(load_power) as load_power,
            avg(pv_power) as pv_power
            from dessdevicedata ddd
            join dmc_devices d on ddd.clientid = d.clientid
            join dmc_units u on d.cabinetserial = u.cabinetserial
            where displayname = %s
            and polldate >= %s
            and polldate < %s
            group by utc_timestamp, utc_date, utc_hour, utc_15min
            order by utc_timestamp, utc_date, utc_hour, utc_15min;""",[displayname,
                                                                     start_date,
                                                                    end_date])
            results = low_res_cursor.fetchall()
            results_df = pd.DataFrame(list(results), columns = ['utc_timestamp', 'utc_date', 'utc_hour', 'utc_15min', 'actual_load_power', 'actual_pv_power']) 
        
            return results_df
        
        results_df = get_actual_data(conn, start_date, tenantname, end_date, displayname)
        
        # Adding the timezone variable. The utc_timestamp column is tz naive and hence had to localize to the given timezone
        results_df['local_time'] = results_df['utc_timestamp'].dt.tz_localize(tz)
        results_df.set_index('local_time', inplace = True)

        
        # Joining the two dataframes on the timestamp axis and then using the final dataframe to plot the graphs
        #final_joined_df = pd.concat([dff, results_df.rename(columns = {'local_time' : 'time'})])
        final_joined_df = pd.merge(results_df, dff, left_index = True, right_index = True)


        
        # Check which tab is selected and show the appropriate graph
        if tab == 'loadTab': 
            # Predicted Load
            traces.append({'x' : final_joined_df.index, 
                            'y' : final_joined_df['load'],
                            'type' : 'line',
                            'name' : 'Forecasted Laod',
                          'color' : 'blue'}) 
            traces.append({'x' : final_joined_df.index, 
                            'y' : final_joined_df['actual_load_power'],
                            'type' : 'line',
                            'name' : 'Actual Laod',
                          'color' : 'orange'}),
                     
        
            figure =  {'data' : traces, 
                    'layout' : go.Layout(title = 'Load Power Prediction Plot',
                                          xaxis = {'title' : 'Time'},
                                          yaxis = {'title' : 'Load Power (W)'},
                                          height = 1400
                                          )
                    }
            return figure
                              # This was part(below 3 lines) of the code that worked. It displays a table with all the correct values.       
                                # return dash_table.DataTable(id = 'table', 
                                #                              columns = [{'name' : j, 'id' : j} for j in forecast_df.columns],
                                #                              data = dff.to_dict('records'),)
        
        elif tab == 'pvTab':
            traces.append({'x' : final_joined_df.index, 
                            'y' : final_joined_df['pv'],
                            'type' : 'line',
                            'name' : 'Forecasted PV',
                            'color' : 'blue'}) 
            traces.append({'x' : final_joined_df.index, 
                            'y' : final_joined_df['actual_pv_power'],
                            'type' : 'line',
                            'name' : 'Actual PV',
                            'color' : 'orange'})                         
        
            figure =  {'data' : traces, 
                    'layout' : go.Layout(title = 'Solar PV Prediction Plot',
                                          xaxis = {'title' : 'Time'},
                                          yaxis = {'title' : 'PV Power (W)'},
                                          height = 1400
                                          )
                    }
                    
            return figure
        else:
            return {'data' : []}
    else: 
        return {'data' : []}
 
    
    

""" 
Callback funciton to calculate the Root Mean Square Error of the Prediction
"""

@app.callback(Output('rmse', 'children'),
                [Input('update-inputs', 'n_clicks')],
                state = [State('my-client', 'value'),
                State('my-date', 'date'),
                State('my-time', 'value'),
                State('hours-ahead', 'value'),
                State('tabs', 'value')],
                )
def get_rmse(n_clicks, Client, Date, Time, HoursAhead, tab):
    if n_clicks:
        RMSE = str(0.99)
        text = "Prediction Accuracy : "
        return text + RMSE
    else: 
        return 0
    


                                    # This was the callback function that actually worked. It was just to test getting data from S3. 
                                    # won't be using going forward but still I am gonna leave it here untill the app is in production. 
                                            
                                    
                                    # @app.callback(Output('tabs-content', 'children'),
                                    #                 [Input('update-button', 'n_clicks')],
                                    #                 state = [State('my-client', 'value'),
                                    #                 State('my-date', 'date'),
                                    #                 State('my-time', 'value'),
                                    #                 State('hoursAhead', 'value'),
                                    #                 State('tabs', 'value')],
                                    #                 )
                                    
                                    # def make_load_chart(n_clicks, Client, Date, Time, HoursAhead, tab):
                                    #     client = boto3.client('s3')
                                    #     resource = boto3.resource('s3')
                                    #     my_bucket = resource.Bucket('demo-sunverge-forecast-power-energy')
                                    #     Clientid = []
                                    #     if Client == 'Tokyo Gas 1':
                                    #         Clientid.append("24d1bcb3-8857-4341-beac-04aa97941787") # Mr. Ishibashi house
                                    #     else : 
                                    #         Clientid.append("5d995db9-8761-4379-ba0f-a8373184a1db") # Intelligent House   
                                        
                                    #     date = Date.strftime('%Y-%m-%d')
                                        
                                    #     Key_Date = date + 'T' + str(Time) + ':00:00'
                                    #     #Key_Date = input("Please enter the date and time of forecast (e.g.2019-11-24T12:00:00 ) : ")
                                        
                                    #     Key = ("clientid=" + '"' + str(Clientid[0]) + '"' + "/createdtime=" + str(Key_Date) + "/power-energy-forecast-" + str(Key_Date) + 'Z.csv.gz')
                                    #     obj = client.get_object(Bucket = 'demo-sunverge-forecast-power-energy', Key = Key)
                                       
                                    #     with gzip.open(obj['Body'], mode = 'rt') as f:
                                    #         forecast_df = pd.read_csv(f)
                                        
                                    #     if tab == 'loadTab': 
                                    #         return html.Div([
                                    #             dash_table.DataTable(
                                    #             columns = [{'name' : i, 'id' : i} for i in df.columns],
                                    #             data = forecast_df.to_dict('records'))  
                                    #             ])
                                    #     else:
                                    #         return html.Div([
                                    #             dash_table.DataTable(
                                    #                 columns = [{'name' : j, 'id' : j} for j in df.columns])
                                    # ]) 
# ------------------------------------------------------------------------------


if __name__ == '__main__':
    app.run_server(debug = True, port = 8055)
